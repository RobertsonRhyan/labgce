# Cloud Computing

## LAB07: Infrastructure-as-Code and Configuration Management - Terraform, Ansible and GitLab

### Group members

**L7GrP**

De Bleser Dimitri, Peer Vincent, Robertson Rhyan

### Description

In this lab you will deploy a web site running on a virtual machine in the cloud 
using tools that adhere to the principles of Infrastructure-as-Code and Desired 
State Configuration. The web server is NGINX and the cloud is Google Cloud.

